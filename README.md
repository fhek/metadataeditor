<!--
    SPDX-FileCopyrightText: 2023 Felipe Kinoshita <kinofhek@gmail.com>
    SPDX-License-Identifier: CC0-1.0
-->

# Metadata Editor

Generate appstream metadata

![metadataeditor window](.gitlab/metadataeditor.png)

Metadata Editor is supposed to be a simple and easy way to create appstream metadata files, without needing to remember every key/value combination from AppStream.

## Build

To build Metadata Editor we're going to use the following instructions:

```bash
$ git clone https://invent.kde.org/fhek/metadataeditor.git
$ cd metadataeditor
$ flatpak-builder --repo=repo build-dir --force-clean .flatpak-manifest.json
$ flatpak build-bundle repo metadataeditor.flatpak org.kde.metadataeditor
```

Now you can either double-click the `metadataeditor.flatpak` file to open it with
some app store (discover, gnome-software, etc...) or run:

```bash
$ flatpak install metadataeditor.flatpak
```
