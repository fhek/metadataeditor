// SPDX-FileCopyrightText: 2023 Felipe Kinoshita <kinofhek@gmail.com>
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include <QObject>
#include <QString>
#include <QUrl>

class Metadata : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QUrl filename MEMBER m_filename NOTIFY filenameChanged)

    Q_PROPERTY(QString identifier MEMBER m_identifier NOTIFY identifierChanged)
    Q_PROPERTY(QString name MEMBER m_name NOTIFY nameChanged)
    Q_PROPERTY(QString summary MEMBER m_summary NOTIFY summaryChanged)

    Q_PROPERTY(QString developerName MEMBER m_developerName NOTIFY developerNameChanged)
    Q_PROPERTY(QString contact MEMBER m_contact NOTIFY contactChanged)

    Q_PROPERTY(QString description MEMBER m_description NOTIFY descriptionChanged)

    Q_PROPERTY(QString metadataLicense MEMBER m_metadataLicense NOTIFY metadataLicenseChanged)
    Q_PROPERTY(QString projectLicense MEMBER m_projectLicense NOTIFY projectLicenseChanged)

    Q_PROPERTY(QString launchable MEMBER m_launchable NOTIFY launchableChanged)
    Q_PROPERTY(QString binary MEMBER m_binary NOTIFY binaryChanged)

    Q_PROPERTY(QStringList screenshots MEMBER m_screenshots NOTIFY screenshotsChanged)
    Q_PROPERTY(QStringList urls MEMBER m_urls NOTIFY urlsChanged)

public:
    explicit Metadata(QObject *parent = nullptr);

    Q_INVOKABLE void open(const QUrl url);
    Q_INVOKABLE void save();

Q_SIGNALS:
    void filenameChanged();

    void metadataChanged();

    void identifierChanged();
    void nameChanged();
    void summaryChanged();

    void developerNameChanged();
    void contactChanged();

    void descriptionChanged();

    void metadataLicenseChanged();
    void projectLicenseChanged();

    void launchableChanged();
    void binaryChanged();

    void screenshotsChanged();
    void urlsChanged();

private:
    QUrl m_filename;

    QString m_identifier;
    QString m_name;
    QString m_summary;

    QString m_developerName;
    QString m_contact;

    QString m_description;

    QString m_metadataLicense;
    QString m_projectLicense;

    QString m_launchable;
    QString m_binary;

    QStringList m_screenshots;
    QStringList m_urls;
};
