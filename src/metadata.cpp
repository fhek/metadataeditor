// SPDX-FileCopyrightText: 2023 Felipe Kinoshita <kinofhek@gmail.com>
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <QDebug>

#include <QDomDocument>
#include <QFile>
#include <QString>
#include <QXmlStreamReader>

#include "metadata.h"

Metadata::Metadata(QObject *parent) : QObject(parent) {}

void Metadata::open(const QUrl filename)
{
    QFile file(filename.toLocalFile());
    if (!file.open(QIODevice::ReadOnly | QFile::Text)) {
        qWarning() << "error reading file" << file.errorString();

        return;
    }

    QXmlStreamReader reader(&file);

    while (!reader.atEnd()) {
        reader.readNext();

        if (reader.isStartDocument()) {
            continue;
        }

        if (reader.isEndDocument()) {
            break;
        }

        if (reader.isStartElement()) {
            QStringView elementName = reader.name();

            if (elementName == QStringLiteral("id")) {
                this->m_identifier = reader.readElementText();
                Q_EMIT identifierChanged();
            }

            if (elementName == QStringLiteral("name")) {
                QXmlStreamAttributes attrs = reader.attributes();

                if (attrs.isEmpty()) { // so we don't get translated fields
                    this->m_name = reader.readElementText();
                    Q_EMIT nameChanged();
                }
            }

            if (elementName == QStringLiteral("summary")) {
                QXmlStreamAttributes attrs = reader.attributes();

                if (attrs.isEmpty()) { // so we don't get translated fields
                    this->m_summary = reader.readElementText();
                    Q_EMIT summaryChanged();
                }
            }

            if (elementName == QStringLiteral("developer_name")) {
                QXmlStreamAttributes attrs = reader.attributes();

                if (attrs.isEmpty()) {
                    this->m_developerName = reader.readElementText();
                    Q_EMIT developerNameChanged();
                }
            }

            if (elementName == QStringLiteral("update_contact")) {
                this->m_contact = reader.readElementText();
                Q_EMIT contactChanged();
            }

            if (elementName == QStringLiteral("metadata_license")) {
                this->m_metadataLicense = reader.readElementText();
                Q_EMIT metadataLicenseChanged();
            }

            if (elementName == QStringLiteral("project_license")) {
                this->m_projectLicense = reader.readElementText();
                Q_EMIT projectLicenseChanged();
            }

            if (elementName == QStringLiteral("launchable")) {
                // this assumes <launchable type="desktop-id">
                this->m_launchable = reader.readElementText();
                Q_EMIT launchableChanged();
            }

            if (elementName == QStringLiteral("provides")) {
                reader.readNextStartElement();

                this->m_binary = reader.readElementText();
                Q_EMIT binaryChanged();
            }

            continue;
        }
    }

    Q_EMIT metadataChanged();

    file.close();
}

void Metadata::save()
{
    QDomDocument document;

    QDomNode xml(document.createProcessingInstruction(QStringLiteral("xml"), QStringLiteral("version=\"1.0\" encoding=\"utf-8\"")));
    document.appendChild(xml);

    // component and application type
    QDomElement component = document.createElement(QStringLiteral("component"));
    component.setAttribute(QStringLiteral("type"), QStringLiteral("desktop-application"));
    document.appendChild(component);

    QDomElement identifier = document.createElement(QStringLiteral("id"));
    identifier.appendChild(document.createTextNode(m_identifier));
    component.appendChild(identifier);

    QDomElement name = document.createElement(QStringLiteral("name"));
    name.appendChild(document.createTextNode(m_name));
    component.appendChild(name);

    QDomElement summary = document.createElement(QStringLiteral("summary"));
    summary.appendChild(document.createTextNode(m_summary));
    component.appendChild(summary);

    QDomElement developerName = document.createElement(QStringLiteral("developer_name"));
    developerName.appendChild(document.createTextNode(m_developerName));
    component.appendChild(developerName);

    QDomElement contact = document.createElement(QStringLiteral("update_contact"));
    contact.appendChild(document.createTextNode(m_contact));
    component.appendChild(contact);

    QDomElement metadataLicense = document.createElement(QStringLiteral("metadata_license"));
    metadataLicense.appendChild(document.createTextNode(m_metadataLicense));
    component.appendChild(metadataLicense);

    QDomElement projectLicense = document.createElement(QStringLiteral("project_license"));
    projectLicense.appendChild(document.createTextNode(m_projectLicense));
    component.appendChild(projectLicense);

    QDomElement launchable = document.createElement(QStringLiteral("launchable"));
    launchable.setAttribute(QStringLiteral("type"), QStringLiteral("desktop-id"));
    launchable.appendChild(document.createTextNode(m_launchable));
    component.appendChild(launchable);

    QDomElement description = document.createElement(QStringLiteral("description"));
    description.appendChild(document.createTextNode(m_description));
    component.appendChild(description);

    // screenshots
    int empty_screenshots = 0;

    for (QString ss : m_screenshots) {
        if (ss.isEmpty()) {
            empty_screenshots++;
            continue;
        }
    }

    if (empty_screenshots != m_screenshots.size()) {
        QDomElement screenshots = document.createElement(QStringLiteral("screenshots"));
        component.appendChild(screenshots);

        for (int i = 0; i < m_screenshots.size(); i++) {
            if (m_screenshots.at(i).isEmpty()) {
                continue;
            }

            QDomElement screenshot = document.createElement(QStringLiteral("screenshot"));
            screenshots.appendChild(screenshot);

            if (i == 0) {
                screenshot.setAttribute(QStringLiteral("type"), QStringLiteral("default"));
            }

            QDomElement image = document.createElement(QStringLiteral("image"));
            screenshot.appendChild(image);
            image.appendChild(document.createTextNode(m_screenshots.at(i)));
        }
    }

    // homepage, bugtracker, and donation urls
    int empty_urls = 0;

    for (QString u : m_urls) {
        if (u.isEmpty()) {
            empty_urls++;
            continue;
        }
    }

    if (empty_urls != m_urls.size()) {
        for (int i = 0; i < m_urls.size(); i++) {
            if (m_urls.at(i).isEmpty()) {
                continue;
            }

            QDomElement url = document.createElement(QStringLiteral("url"));
            component.appendChild(url);

            url.appendChild(document.createTextNode(m_urls.at(i)));

            if (i == 0) {
                url.setAttribute(QStringLiteral("type"), QStringLiteral("homepage"));
            }
            if (i == 1) {
                url.setAttribute(QStringLiteral("type"), QStringLiteral("bugtracker"));
            }
            if (i == 2) {
                url.setAttribute(QStringLiteral("type"), QStringLiteral("donation"));
            }
        }
    }

    // binary file

    QDomElement provides = document.createElement(QStringLiteral("provides"));
    component.appendChild(provides);

    QDomElement binary = document.createElement(QStringLiteral("binary"));
    binary.appendChild(document.createTextNode(m_binary));
    provides.appendChild(binary);

    // save file

    QFile file(m_filename.toLocalFile());
    file.open(QIODevice::WriteOnly);
    file.write(document.toByteArray(2)); // TODO: make this configurable
    file.close();
}
