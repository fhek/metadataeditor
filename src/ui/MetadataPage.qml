// SPDX-FileCopyrightText: 2023 Felipe Kinoshita <kinofhek@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15
import Qt.labs.platform 1.1

import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

import org.kde.metadataeditor 1.0

QQC2.Page {
    id: control

    FileDialog {
        id: openDialog

        onAccepted: metadata.open(currentFile)
    }

    FileDialog {
        id: fileDialog

        fileMode: FileDialog.SaveFile
        onAccepted: metadata.filename = currentFile
    }

    Metadata {
        id: metadata

        identifier: identifier.text
        name: name.text
        summary: summary.text

        developerName: developerName.text
        contact: contact.text

        description: description.text

        metadataLicense: metadataLicense.text
        projectLicense: applicationLicense.text

        launchable: launchableTextField.text
        binary: providesTextField.text

        screenshots: [defaultScreenshot.text, firstAdditionalScreenshot.text, secondAdditionalScreenshot.text]
        urls: [homepage.text, bugtracker.text, donation.text]

        onMetadataChanged: {
            identifier.text = metadata.identifier
            name.text = metadata.name
            summary.text = metadata.summary

            developerName.text = metadata.developerName
            contact.text = metadata.contact

            metadataLicense.text = metadata.metadataLicense
            applicationLicense.text = metadata.projectLicense

            launchableTextField.text = metadata.launchable
            providesTextField.text = metadata.binary
        }

        onFilenameChanged: metadata.save()
    }

    header: QQC2.ToolBar {
        RowLayout {
        anchors.fill: parent

        QQC2.ToolButton {
            action: Kirigami.Action {
                text: i18n("Open…")
                icon.name: "document-open"
                shortcut: StandardKey.Open
                onTriggered: openDialog.open()
            }
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.ToolButton {
            action: Kirigami.Action {
                text: i18n("Save As…")
                icon.name: "document-save-as"
                shortcut: StandardKey.SaveAs
                onTriggered: fileDialog.open()
            }
        }
        }
    }

    contentItem: QQC2.ScrollView {
        ColumnLayout {
            anchors.fill: parent

            Item {
                Layout.maximumHeight: Kirigami.Units.gridUnit
                Layout.minimumHeight: Kirigami.Units.gridUnit
            }

            MobileForm.FormCard {
                Layout.fillWidth: true
                // Layout.topMargin: Kirigami.Units.gridUnit

                contentItem: ColumnLayout {
                    spacing: 0

                    MobileForm.FormCardHeader {
                        title: i18n("Basic Information")
                    }

                    MobileForm.FormTextFieldDelegate {
                        id: identifier

                        label: i18n("Application ID")
                        placeholderText: i18n("A Reverse-DNS string identifying your application")
                    }
                    MobileForm.FormTextFieldDelegate {
                        id: name

                        label: i18n("Application Name")
                        placeholderText: i18n("The human-readable name of your application")
                    }
                    MobileForm.FormTextFieldDelegate {
                        id: summary

                        label: i18n("Summary")
                        placeholderText: i18n("A short summary of what your application does")
                    }

                    MobileForm.FormDelegateSeparator {}

                    MobileForm.AbstractFormDelegate {
                        id: description

                        contentItem: ColumnLayout {
                            QQC2.Label {
                                Layout.fillWidth: true

                                text: i18n("Description")
                            }
                            QQC2.TextArea {
                                id: textarea

                                Layout.fillWidth: true
                                Layout.minimumHeight: Kirigami.Units.gridUnit * 6

                                placeholderText: i18n("A long description of your application")

                                onEditingFinished: description.text = textarea.text
                            }
                        }

                        background: Item {}
                    }
                }
            }

            MobileForm.FormCard {
                id: files

                Layout.fillWidth: true
                Layout.topMargin: Kirigami.Units.largeSpacing

                contentItem: ColumnLayout {
                    spacing: 0

                    MobileForm.FormCardHeader {
                        title: i18n("Files")
                    }
                    MobileForm.FormTextFieldDelegate {
                        id: launchableTextField

                        label: i18n("Name of your .desktop file")
                        placeholderText: i18n("Software Centers you use it to know how to launch your application")
                    }

                    MobileForm.FormDelegateSeparator {}

                    MobileForm.FormTextFieldDelegate {
                        id: providesTextField

                        label: i18n("Name of the binary your application provides")
                        placeholderText: i18n("Makes your binary known to distributions")
                    }
                }
            }

            MobileForm.FormCard {
                Layout.fillWidth: true
                Layout.topMargin: Kirigami.Units.largeSpacing

                contentItem: ColumnLayout {
                    spacing: 0

                    MobileForm.FormCardHeader {
                        title: i18n("Licenses")
                    }


                    MobileForm.FormTextFieldDelegate {
                        id: applicationLicense

                        label: i18n("Application License")
                        placeholderText: i18n("The license of your application")
                    }
                    MobileForm.FormTextFieldDelegate {
                        id: metadataLicense

                        label: i18n("Metadata License")
                        placeholderText: i18n("The license of the generated metadata file")
                    }
                }
            }

            MobileForm.FormCard {
                Layout.fillWidth: true
                Layout.topMargin: Kirigami.Units.largeSpacing

                contentItem: ColumnLayout {
                    spacing: 0

                    MobileForm.FormCardHeader {
                        title: i18n("Screenshots")
                    }

                    MobileForm.FormTextFieldDelegate {
                        id: defaultScreenshot

                        label: i18n("Default")
                        placeholderText: i18n("The URL of the primary screenshot")
                    }
                    MobileForm.FormTextFieldDelegate {
                        id: firstAdditionalScreenshot

                        label: i18n("Additional Screenshot 1")
                        placeholderText: i18n("The URL of the additional screenshot")
                    }
                    MobileForm.FormTextFieldDelegate {
                        id: secondAdditionalScreenshot

                        label: i18n("Additional Screenshot 2")
                        placeholderText: i18n("The URL of the additional screenshot")
                    }
                }
            }

            MobileForm.FormCard {
                Layout.fillWidth: true
                Layout.topMargin: Kirigami.Units.largeSpacing

                contentItem: ColumnLayout {
                    spacing: 0

                    MobileForm.FormCardHeader {
                        title: i18n("Links")
                    }

                    MobileForm.FormTextFieldDelegate {
                        id: homepage

                        label: i18n("Homepage")
                        placeholderText: i18n("An URL to your application's homepage")
                    }
                    MobileForm.FormTextFieldDelegate {
                        id: bugtracker

                        label: i18n("Bug Tracker")
                        placeholderText: i18n("An URL to your application's bug tracking system")
                    }
                    MobileForm.FormTextFieldDelegate {
                        id: donation

                        label: i18n("Donation")
                        placeholderText: i18n("An URL to your application's donation page")
                    }
                }
            }

            MobileForm.FormCard {
                Layout.fillWidth: true
                Layout.topMargin: Kirigami.Units.largeSpacing

                contentItem: ColumnLayout {
                    spacing: 0

                    MobileForm.FormCardHeader {
                        title: i18n("Developer Information")
                    }

                    MobileForm.FormTextFieldDelegate {
                        id: developerName

                        label: i18n("Developer Name")
                        placeholderText: i18n("The developers or project responsible for this application")
                    }

                    MobileForm.FormTextFieldDelegate {
                        id: contact

                        label: i18n("Contact")
                        placeholderText: i18n("An email address to contact the project")
                    }

                    MobileForm.FormSectionText {
                        text: i18n("Distributors can use this email to contact the project about invalid or incomplete metadata. It can also be used to ask general questions in case of an update to the component described in the metadata file.")
                    }
                }
            }

            Item {
                Layout.maximumHeight: Kirigami.Units.gridUnit
                Layout.minimumHeight: Kirigami.Units.gridUnit
            }
        }
    }
}
