// SPDX-FileCopyrightText: 2023 Felipe Kinoshita <kinofhek@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

import org.kde.metadataeditor 1.0

QQC2.Page {
    id: control

    property string iconName
    property alias action: button.action

    ColumnLayout {
        anchors.fill: parent

        spacing: 0

        Item {
            Layout.fillHeight: true
        }

        Loader {
            id: imageLoader

            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: active ? Kirigami.Units.iconSizes.enormous : 0
            Layout.preferredHeight: active ? Kirigami.Units.iconSizes.enormous : 0
            Layout.bottomMargin: active ? Kirigami.Units.gridUnit * 2 : 0

            active: control.iconName != ""
            sourceComponent: Image {
                id: image

                Layout.preferredWidth: Kirigami.Units.iconSizes.enormous
                Layout.preferredHeight: Kirigami.Units.iconSizes.enormous
                Layout.alignment: Qt.AlignCenter
                Layout.bottomMargin: Kirigami.Units.gridUnit * 2

                source: control.iconName
                smooth: true
            }
        }

        QQC2.Label {
            Layout.maximumWidth: parent.width
            Layout.alignment: Qt.AlignCenter

            text: i18n("Welcome to %1", About.displayName)
            font.pointSize: 24
            font.bold: true
            horizontalAlignment: Qt.AlignCenter

            wrapMode: Text.Wrap
        }
        QQC2.Label {
            Layout.alignment: Qt.AlignCenter

            text: About.shortDescription
        }

        QQC2.Button {
            id: button

            Layout.alignment: Qt.AlignCenter
            Layout.topMargin: Kirigami.Units.gridUnit * 2
        }

        Item {
            Layout.fillHeight: true
        }
    }

    background: Rectangle {
        Kirigami.Theme.colorSet: Kirigami.Theme.Header
        Kirigami.Theme.inherit: false
        color: Kirigami.Theme.backgroundColor
    }
}
