// SPDX-FileCopyrightText: 2023 Felipe Kinoshita <kinofhek@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

import org.kde.metadataeditor 1.0

QQC2.ApplicationWindow {
    id: root

    visible: true
    title: i18n("Metadata Editor")

    width: Kirigami.Units.gridUnit * 41
    height: Kirigami.Units.gridUnit * 37
    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }

    Connections {
        id: saveWindowGeometryConnections
        enabled: !Kirigami.Settings.isMobile
        target: root

        function onClosing() { App.saveWindowGeometry(root); }
        function onWidthChanged() { saveWindowGeometryTimer.restart(); }
        function onHeightChanged() { saveWindowGeometryTimer.restart(); }
        function onXChanged() { saveWindowGeometryTimer.restart(); }
        function onYChanged() { saveWindowGeometryTimer.restart(); }
    }

    Component {
        id: welcomePage

        WelcomePage {
            iconName: Qt.resolvedUrl("metadataeditor.svg")

            action: Kirigami.Action {
                text: i18n("Start")
                onTriggered: {
                    stackview.push(Qt.resolvedUrl("MetadataPage.qml"))
                    Config.firstStart = false
                    Config.save()
                }
            }
        }
    }

    QQC2.StackView {
        id: stackview

        anchors.fill: parent

        initialItem: Config.firstStart ? welcomePage : Qt.resolvedUrl("MetadataPage.qml")

        pushExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to: 0
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
        pushEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
    }
}
