// SPDX-FileCopyrightText: 2023 Felipe Kinoshita <kinofhek@gmail.com>
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <QtGlobal>
#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#else
#include <QApplication>
#endif
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQuickWindow>
#include <QUrl>
#include <QtQml>

#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>
#ifdef HAVE_KDBUSADDONS
#include <KDBusService>
#endif

constexpr auto APPLICATION_ID = "org.kde.metadataeditor";

#include "app.h"
#include "config.h"
#include "metadata.h"
#include "version-metadataeditor.h"

#ifdef Q_OS_ANDROID
Q_DECL_EXPORT
#endif
int main(int argc, char *argv[]) {
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

#ifdef Q_OS_ANDROID
  QGuiApplication app(argc, argv);
  QQuickStyle::setStyle(QStringLiteral("org.kde.breeze"));
#else
  QApplication app(argc, argv);
  // Default to org.kde.desktop style unless the user forces another style
  if (qEnvironmentVariableIsEmpty("QT_QUICK_CONTROLS_STYLE")) {
    QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
  }
#endif

#ifdef Q_OS_WINDOWS
  if (AttachConsole(ATTACH_PARENT_PROCESS)) {
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);
  }

  QApplication::setStyle(QStringLiteral("breeze"));
  auto font = app.font();
  font.setPointSize(10);
  app.setFont(font);
#endif

  KLocalizedString::setApplicationDomain("metadataeditor");

  QCoreApplication::setOrganizationName(QStringLiteral("KDE"));

  KAboutData aboutData(QStringLiteral("metadataeditor"),
                       i18nc("@title", "Metadata Editor"),
                       QStringLiteral(METADATAEDITOR_VERSION_STRING),
                       i18n("Generate appstream metadata with ease"), KAboutLicense::GPL,
                       i18n("© 2023 Felipe Kinoshita"));
  aboutData.addAuthor(i18nc("@info:credit", "Felipe Kinoshita"),
                      i18nc("@info:credit", "Author"),
                      QStringLiteral("kinofhek@gmail.com"),
                      QStringLiteral("https://fhek.gitlab.io"));
  aboutData.setTranslator(i18nc("NAME OF TRANSLATORS", "Your names"),
                          i18nc("EMAIL OF TRANSLATORS", "Your emails"));
  aboutData.setBugAddress(
      "https://invent.kde.org/fhek/metadataeditor/-/issues/new");
  KAboutData::setApplicationData(aboutData);
  QGuiApplication::setWindowIcon(
      QIcon::fromTheme(QStringLiteral("org.kde.metadataeditor")));

  QQmlApplicationEngine engine;

  qmlRegisterSingletonType(APPLICATION_ID, 1, 0, "About",
                           [](QQmlEngine *engine, QJSEngine *) -> QJSValue {
                             return engine->toScriptValue(
                                 KAboutData::applicationData());
                           });

  App application;
  auto config = Config::self();
  qmlRegisterSingletonInstance(APPLICATION_ID, 1, 0, "App", &application);
  qmlRegisterSingletonInstance(APPLICATION_ID, 1, 0, "Config", config);

  qmlRegisterType<Metadata>(APPLICATION_ID, 1, 0, "Metadata");

  engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
  engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

  if (engine.rootObjects().isEmpty()) {
    return -1;
  }

#ifdef HAVE_KDBUSADDONS
  KDBusService service(KDBusService::Unique);
#endif

  // Restore window size and position
  const auto rootObjects = engine.rootObjects();
  for (auto obj : rootObjects) {
    auto view = qobject_cast<QQuickWindow *>(obj);
    if (view) {
      if (view->isVisible()) {
        application.restoreWindowGeometry(view);
      }
      break;
    }
  }

  return app.exec();
}
